*Read and share online: <https://blogs.gnome.org/engagement/2020/09/30/friends-of-gnome-update-september-2020/>*

# Welcome to the September 2020 edition of Friends of GNOME Update!

## GNOME 3.38 Orbis is out!

We released GNOME 3.38 Orbis! The release, of course, includes an amazing release video we highly recommend [checking out][1]. Release notes are [available online][2].

[1]: https://www.youtube.com/watch?v=DZ_P5W9r2JY
[2]: https://help.gnome.org/misc/release-notes/3.38/

## GNOME on the Road

Several Foundation staff presented at [GNOME Africa Onboard Virtual][3]. Kristi Progri helped kick off the event with Foundation vice-president Regina Nkemchor Adejo. M de Blanc and Rosanna Yuen talked about the GNOME code of conduct. Melissa Wu reprised her session on [What it’s Like to Be New to GNOME][4].

[3]: https://events.gnome.org/event/30/
[4]: https://www.youtube.com/watch?v=2dQyaA5_Zr8

Rosanna will also be presenting at [All Things Open][5]. On October 20 at 3:30pm ET, you can catch “[GNOME Foundation Then and Now — 20 years of bringing free software to the desktop][6].”

[5]: https://2020.allthingsopen.org
[6]: https://2020.allthingsopen.org/sessions/gnome-foundation-then-and-now-20-years-of-bringing-free-software-to-the-desktop/

## Community Education Challenge

Exciting things are happening with the [Community Education Challenge][7]! While the [phase one winners][8] work on their projects, our [fabulous judges][9] have been hosting office hours to discuss the Challenge. To keep up with office hours and other Challenge news, [sign up for the email list][10].

[7]: https://www.gnome.org/challenge/
[8]: https://www.gnome.org/challenge/phase-one-winners/
[9]: https://www.gnome.org/challenge/judges/
[10]: https://surveys.gnome.org/index.php/671583?lang=en%20noopener%20noreferrer

## GNOME.Asia

We’ve been working with the local team on [GNOME.Asia][11]. In addition to other developments, the Call for Proposals is open. You can [submit to the CfP until 18 October 2020][12]. GNOME.Asia 2020 will be taking place online. 

[11]: https://events.gnome.org/event/24/
[12]: https://events.gnome.org/event/24/abstracts/

## Grants Strategy

We want to help you fund your GNOME projects! While the Foundation is not giving out grants, we are helping with grant applications for specific parts of the project. If you have any ideas, please [add them to the wiki][13].

[13]: https://gitlab.gnome.org/Teams/Engagement/Fundraising/-/wikis/Fundable-Projects

## Annual Report

Each year the GNOME Foundation produces an [annual report][14]. This report covers Foundation and community activities over the past year. This year's report is now underway. 

[14]: https://www.gnome.org/foundation/reports/

## Thank you!

Thank you for being a [Friend of GNOME][15] and supporting everything we do!

[15]: https://gnome.org/friends