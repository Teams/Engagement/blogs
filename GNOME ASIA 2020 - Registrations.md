GNOME ASIA 2020 - Registrations are now open

We’re excited to announce that the Registrations for [GNOME Asia Summit 2020][1] are open! You can now [register online][2].

[1]: https://summit.gnome.my
[2]: https://events.gnome.org/event/24/registrations/21/

GNOME Asia Summit 2020 will take place online November 24 - 26.

Topics covered include the GNOME desktop and a range of other topics that are GNOME specific and general to the free software and tech communities. The summit brings together the GNOME community in Asia to provide a place for users, developers, leaders, governments and businesses to discuss present technology and future developments.

More information about the GNOME Asia 2020 Summit including is available on the [official website][3]. The GNOME Asia Summit will be three days of stand out keynotes, engaging and educational sessions, and skill building Birds of a Feather sessions and workshops, so [register today][4].

[3]: https://summit.gnome.my
[4]: https://events.gnome.org/event/24/registrations/21/