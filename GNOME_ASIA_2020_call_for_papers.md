title: GNOME ASIA 2020 call for papers is now open
date: 2020-29-09
tags:GNOME, Community, Events, GNOME ASIA, 2020, News post

GNOME.Asia Summit 2020 invites you to participate as a speaker at the conference on the 24th and 26th of November 2020 by [submitting your abstract][1].

[1]: https://events.gnome.org/event/24/abstracts/

GNOME.Asia Summit is the featured annual GNOME conference in Asia. It focuses primarily on the GNOME desktop, and also covers applications and platform development tools. The Summit brings together the GNOME community in Asia to provide a forum for users, developers, foundation leaders, governments and businesses to discuss the present technology and future developments.

**The possible topics are, but not limited to :**

* Contributing to GNOME
* UI design
* Accessibility
* Human Interface Engineering (Icons and Graphic Design)
* Marketing/Engagement
* Developing GNOME on embedded systems or open hardware
* Contributing to Linux and FLOSS
* Linux kernel and development
* The development and promotion of open-source operating systems
* Distributions, including Debian, Fedora, openSUSE, Ubuntu, and FreeBSD
* The development and promotion of other open source projects
* We are also interested in other topics related to Free/Libre and Open Source Software which are not listed above.

Session will be scheduled for 25 or 50 minutes (Q&A included). The session could be a technical talk, panel discussion, or Birds of a Feather session (BoF). If you need more time or additional resources, feel free to get in touch with the organizing team.

**Submission Criteria**

Please provide a short abstract of your presentation (under 200 words). Also include your name, biographical information, title, and desired length of session. Please submit your proposal to the following link: <https://events.gnome.org/event/24/abstracts/>.

The reviewers team will evaluate the entries based on the submitted abstracts and available time by following the schedule.

**Submission timeline: September 25th 2020**

**Confirmation of paper acceptance: October 18th 2020**

